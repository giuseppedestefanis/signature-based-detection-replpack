This test context is a round trip where the aircraft takeoff and land at Ellington airport in Texas. This is the default JSBsim flight scenario for the Cessna C310.

Each fault injection scenario:
   * Contains the mutated aircraft model (.xml file).
   * Contains the raw scenario run data (*_raw.csv file).
   * Contains the evaluation results for each output metric (*_eval.csv file). Note that these evaluation results cover different features from which we could determine precision and recall.