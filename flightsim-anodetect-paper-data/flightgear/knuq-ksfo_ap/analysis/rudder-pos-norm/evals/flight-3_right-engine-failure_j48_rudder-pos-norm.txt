Summary:

Correctly Classified Instances         137               94.4828 %
Incorrectly Classified Instances         8                5.5172 %
Kappa statistic                          0.8864
Mean absolute error                      0.0465
Root mean squared error                  0.1878
Relative absolute error                 10.902  %
Root relative squared error             41.4019 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 53  2  1 |  a = Low
  0  0  0 |  b = Medium
  5  0 84 |  c = High

--------------
True Positive Rate:
0.9448275862068966
--------------
Precision:0.9594852066867174  Recall:0.9448275862068966