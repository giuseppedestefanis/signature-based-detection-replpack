Summary:

Correctly Classified Instances          87               60      %
Incorrectly Classified Instances        58               40      %
Kappa statistic                          0.4121
Mean absolute error                      0.3227
Root mean squared error                  0.4286
Relative absolute error                 72.5888 %
Root relative squared error             90.9058 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 24 11 21 |  a = Low
  0 30 16 |  b = Medium
  2  8 33 |  c = High

--------------
True Positive Rate:
0.6
--------------
Precision:0.6905310453093705  Recall:0.6