Summary:

Correctly Classified Instances         106               73.1034 %
Incorrectly Classified Instances        39               26.8966 %
Kappa statistic                          0.5965
Mean absolute error                      0.2691
Root mean squared error                  0.3668
Relative absolute error                 60.5603 %
Root relative squared error             77.8205 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 28  8 12 |  a = Low
  3 38  8 |  b = Medium
  5  3 40 |  c = High

--------------
True Positive Rate:
0.7310344827586207
--------------
Precision:0.7402298850574713  Recall:0.7310344827586207