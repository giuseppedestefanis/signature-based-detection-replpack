Summary:

Correctly Classified Instances         142               97.931  %
Incorrectly Classified Instances         3                2.069  %
Kappa statistic                          0.9689
Mean absolute error                      0.0138
Root mean squared error                  0.1174
Relative absolute error                  3.1038 %
Root relative squared error             24.916  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 47  1  0 |  a = Low
  1 49  0 |  b = Medium
  0  1 46 |  c = High

--------------
True Positive Rate:
0.9793103448275862
--------------
Precision:0.9795807978363759  Recall:0.9793103448275862