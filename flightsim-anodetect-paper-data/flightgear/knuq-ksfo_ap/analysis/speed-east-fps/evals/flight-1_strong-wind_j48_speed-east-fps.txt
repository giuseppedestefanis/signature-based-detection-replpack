Summary:

Correctly Classified Instances          73               50.3448 %
Incorrectly Classified Instances        72               49.6552 %
Kappa statistic                          0.2794
Mean absolute error                      0.331 
Root mean squared error                  0.5754
Relative absolute error                 74.5435 %
Root relative squared error            122.1483 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 37  0  0 |  a = Low
 32  8 20 |  b = Medium
  0 20 28 |  c = High

--------------
True Positive Rate:
0.503448275862069
--------------
Precision:0.4481616334689798  Recall:0.503448275862069