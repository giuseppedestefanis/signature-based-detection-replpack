Summary:

Correctly Classified Instances         142               97.931  %
Incorrectly Classified Instances         3                2.069  %
Kappa statistic                          0.969 
Mean absolute error                      0.0138
Root mean squared error                  0.1174
Relative absolute error                  3.1032 %
Root relative squared error             24.9108 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 49  1  0 |  a = Low
  0 47  0 |  b = Medium
  0  2 46 |  c = High

--------------
True Positive Rate:
0.9793103448275862
--------------
Precision:0.9805517241379311  Recall:0.9793103448275862