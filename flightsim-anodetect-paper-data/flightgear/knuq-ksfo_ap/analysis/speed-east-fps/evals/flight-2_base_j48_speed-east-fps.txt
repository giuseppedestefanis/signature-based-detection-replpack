Summary:

Correctly Classified Instances         144               99.3103 %
Incorrectly Classified Instances         1                0.6897 %
Kappa statistic                          0.9897
Mean absolute error                      0.0046
Root mean squared error                  0.0678
Relative absolute error                  1.0345 %
Root relative squared error             14.3842 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 47  0  0 |  a = Low
  1 48  0 |  b = Medium
  0  0 49 |  c = High

--------------
True Positive Rate:
0.993103448275862
--------------
Precision:0.9932471264367815  Recall:0.993103448275862