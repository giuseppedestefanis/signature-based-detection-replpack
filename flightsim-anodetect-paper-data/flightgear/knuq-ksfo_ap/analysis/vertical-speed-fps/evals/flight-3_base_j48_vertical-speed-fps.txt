Summary:

Correctly Classified Instances         121               83.4483 %
Incorrectly Classified Instances        24               16.5517 %
Kappa statistic                          0.7521
Mean absolute error                      0.1792
Root mean squared error                  0.3044
Relative absolute error                 40.2952 %
Root relative squared error             64.5368 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 41  8  0 |  a = Low
  4 36  2 |  b = Medium
  4  6 44 |  c = High

--------------
True Positive Rate:
0.8344827586206897
--------------
Precision:0.8475322338830584  Recall:0.8344827586206897