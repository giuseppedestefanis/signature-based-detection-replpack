Summary:

Correctly Classified Instances          31               21.3793 %
Incorrectly Classified Instances       114               78.6207 %
Kappa statistic                          0.0493
Mean absolute error                      0.5241
Root mean squared error                  0.724 
Relative absolute error                117.5749 %
Root relative squared error            153.1115 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 23  0  0 |  a = Low
  5  0  0 |  b = Medium
 84 25  8 |  c = High

--------------
True Positive Rate:
0.21379310344827587
--------------
Precision:0.8394704433497537  Recall:0.21379310344827587