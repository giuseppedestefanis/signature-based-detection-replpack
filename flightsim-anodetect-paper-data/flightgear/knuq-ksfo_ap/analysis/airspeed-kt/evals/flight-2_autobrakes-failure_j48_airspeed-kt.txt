Summary:

Correctly Classified Instances         125               86.2069 %
Incorrectly Classified Instances        20               13.7931 %
Kappa statistic                          0.791 
Mean absolute error                      0.092 
Root mean squared error                  0.3032
Relative absolute error                 20.6661 %
Root relative squared error             64.252  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 45  0  0 |  a = Low
  4 28  0 |  b = Medium
  0 16 52 |  c = High

--------------
True Positive Rate:
0.8620689655172413
--------------
Precision:0.89441494466125  Recall:0.8620689655172413