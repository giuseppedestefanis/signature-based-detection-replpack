Summary:

Correctly Classified Instances          38               26.2069 %
Incorrectly Classified Instances       107               73.7931 %
Kappa statistic                         -0.1012
Mean absolute error                      0.4848
Root mean squared error                  0.6344
Relative absolute error                109.0107 %
Root relative squared error            134.4704 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 14  9 15 |  a = Low
  3 14 21 |  b = Medium
 36 23 10 |  c = High

--------------
True Positive Rate:
0.2620689655172414
--------------
Precision:0.2524341602783514  Recall:0.2620689655172414