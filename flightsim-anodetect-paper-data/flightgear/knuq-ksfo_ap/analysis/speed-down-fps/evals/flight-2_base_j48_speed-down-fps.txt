Summary:

Correctly Classified Instances          91               62.7586 %
Incorrectly Classified Instances        54               37.2414 %
Kappa statistic                          0.4409
Mean absolute error                      0.2784
Root mean squared error                  0.4399
Relative absolute error                 62.7049 %
Root relative squared error             93.3911 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 45  6 18 |  a = Low
  3 37 21 |  b = Medium
  0  6  9 |  c = High

--------------
True Positive Rate:
0.6275862068965518
--------------
Precision:0.7831808585503167  Recall:0.6275862068965518