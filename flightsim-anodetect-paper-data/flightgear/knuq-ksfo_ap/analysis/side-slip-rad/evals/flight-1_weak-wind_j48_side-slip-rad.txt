Summary:

Correctly Classified Instances          43               29.6552 %
Incorrectly Classified Instances       102               70.3448 %
Kappa statistic                          0.0836
Mean absolute error                      0.408 
Root mean squared error                  0.5361
Relative absolute error                 91.6295 %
Root relative squared error            113.5012 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
  2 39  0 |  a = Low
  0 21  0 |  b = Medium
 28 35 20 |  c = High

--------------
True Positive Rate:
0.296551724137931
--------------
Precision:0.6232788868723532  Recall:0.296551724137931