Summary:

Correctly Classified Instances          75               51.7241 %
Incorrectly Classified Instances        70               48.2759 %
Kappa statistic                          0.2078
Mean absolute error                      0.3487
Root mean squared error                  0.5392
Relative absolute error                 78.2144 %
Root relative squared error            114.0317 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 48  7 15 |  a = Low
  2  1  1 |  b = Medium
 23 22 26 |  c = High

--------------
True Positive Rate:
0.5172413793103449
--------------
Precision:0.6214697347999191  Recall:0.5172413793103449