Summary:

Correctly Classified Instances         104               71.7241 %
Incorrectly Classified Instances        41               28.2759 %
Kappa statistic                          0.4583
Mean absolute error                      0.1885
Root mean squared error                  0.4342
Relative absolute error                 42.4336 %
Root relative squared error             92.1427 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 76 14  0 |  a = Low
 10 28 17 |  b = Medium
  0  0  0 |  c = High

--------------
True Positive Rate:
0.7172413793103448
--------------
Precision:0.8013900026730821  Recall:0.7172413793103448