Summary:

Correctly Classified Instances          46               31.7241 %
Incorrectly Classified Instances        99               68.2759 %
Kappa statistic                          0     
Mean absolute error                      0.4552
Root mean squared error                  0.6747
Relative absolute error                102.069  %
Root relative squared error            142.6337 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 46 31 68 |  a = Low
  0  0  0 |  b = Medium
  0  0  0 |  c = High

--------------
True Positive Rate:
0.31724137931034485
--------------
Precision:1.0  Recall:0.31724137931034485