Summary:

Correctly Classified Instances          94               64.8276 %
Incorrectly Classified Instances        51               35.1724 %
Kappa statistic                          0.4821
Mean absolute error                      0.2345
Root mean squared error                  0.4842
Relative absolute error                 52.872  %
Root relative squared error            102.9401 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
  1  0  0 |  a = Low
 47 28  4 |  b = Medium
  0  0 65 |  c = High

--------------
True Positive Rate:
0.6482758620689655
--------------
Precision:0.9672601199400299  Recall:0.6482758620689655