Summary:

Correctly Classified Instances          93               64.1379 %
Incorrectly Classified Instances        52               35.8621 %
Kappa statistic                          0.4296
Mean absolute error                      0.2391
Root mean squared error                  0.489 
Relative absolute error                 53.7505 %
Root relative squared error            103.6393 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 61  3  0 |  a = Low
 20 16  1 |  b = Medium
  5 23 16 |  c = High

--------------
True Positive Rate:
0.6413793103448275
--------------
Precision:0.6958782871608948  Recall:0.6413793103448275