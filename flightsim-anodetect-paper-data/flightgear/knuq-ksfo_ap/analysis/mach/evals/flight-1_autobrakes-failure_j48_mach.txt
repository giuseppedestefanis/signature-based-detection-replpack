Summary:

Correctly Classified Instances         140               96.5517 %
Incorrectly Classified Instances         5                3.4483 %
Kappa statistic                          0.9457
Mean absolute error                      0.023 
Root mean squared error                  0.1516
Relative absolute error                  5.1672 %
Root relative squared error             32.1305 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 45  0  0 |  a = Low
  1 29  4 |  b = Medium
  0  0 66 |  c = High

--------------
True Positive Rate:
0.9655172413793104
--------------
Precision:0.9672435210965945  Recall:0.9655172413793104