Summary:

Correctly Classified Instances         119               82.069  %
Incorrectly Classified Instances        26               17.931  %
Kappa statistic                          0.7325
Mean absolute error                      0.149 
Root mean squared error                  0.3316
Relative absolute error                 33.5506 %
Root relative squared error             70.3886 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 39  0  0 |  a = Low
  9 38 11 |  b = Medium
  2  4 42 |  c = High

--------------
True Positive Rate:
0.8206896551724138
--------------
Precision:0.8340270781051522  Recall:0.8206896551724138