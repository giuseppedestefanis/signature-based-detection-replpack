Summary:

Correctly Classified Instances         127               87.5862 %
Incorrectly Classified Instances        18               12.4138 %
Kappa statistic                          0.8111
Mean absolute error                      0.1233
Root mean squared error                  0.278 
Relative absolute error                 27.7671 %
Root relative squared error             59.0155 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 42  2  1 |  a = Low
  2 52  5 |  b = Medium
  2  6 33 |  c = High

--------------
True Positive Rate:
0.8758620689655172
--------------
Precision:0.8752592934302079  Recall:0.8758620689655172