Summary:

Correctly Classified Instances         117               80.6897 %
Incorrectly Classified Instances        28               19.3103 %
Kappa statistic                          0.7092
Mean absolute error                      0.16  
Root mean squared error                  0.3501
Relative absolute error                 36.0178 %
Root relative squared error             74.3221 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 41  3  2 |  a = Low
  8 42  8 |  b = Medium
  1  6 34 |  c = High

--------------
True Positive Rate:
0.8068965517241379
--------------
Precision:0.8080449935460077  Recall:0.8068965517241379