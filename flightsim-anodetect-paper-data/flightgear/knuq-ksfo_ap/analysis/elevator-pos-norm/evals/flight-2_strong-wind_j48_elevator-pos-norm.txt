Summary:

Correctly Classified Instances         123               84.8276 %
Incorrectly Classified Instances        22               15.1724 %
Kappa statistic                          0.7713
Mean absolute error                      0.1431
Root mean squared error                  0.2982
Relative absolute error                 32.1975 %
Root relative squared error             63.2597 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 40  0  0 |  a = Low
 11 31  6 |  b = Medium
  0  5 52 |  c = High

--------------
True Positive Rate:
0.8482758620689655
--------------
Precision:0.8538574526124362  Recall:0.8482758620689655