Summary:

Correctly Classified Instances         108               74.4828 %
Incorrectly Classified Instances        37               25.5172 %
Kappa statistic                          0.6253
Mean absolute error                      0.1922
Root mean squared error                  0.3763
Relative absolute error                 43.3108 %
Root relative squared error             79.9309 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 53  0  0 |  a = Low
 12 30 25 |  b = Medium
  0  0 25 |  c = High

--------------
True Positive Rate:
0.7448275862068966
--------------
Precision:0.84631299734748  Recall:0.7448275862068966