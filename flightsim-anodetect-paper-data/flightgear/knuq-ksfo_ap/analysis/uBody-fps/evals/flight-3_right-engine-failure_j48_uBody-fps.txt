Summary:

Correctly Classified Instances         126               86.8966 %
Incorrectly Classified Instances        19               13.1034 %
Kappa statistic                          0.7983
Mean absolute error                      0.0874
Root mean squared error                  0.2956
Relative absolute error                 19.641  %
Root relative squared error             62.6513 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 38  3  0 |  a = Low
 11 27  0 |  b = Medium
  0  5 61 |  c = High

--------------
True Positive Rate:
0.8689655172413793
--------------
Precision:0.876622097114708  Recall:0.8689655172413793