Summary:

Correctly Classified Instances          94               64.8276 %
Incorrectly Classified Instances        51               35.1724 %
Kappa statistic                          0.4529
Mean absolute error                      0.2502
Root mean squared error                  0.4076
Relative absolute error                 56.2943 %
Root relative squared error             86.4742 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 50  1 11 |  a = Low
 10 29 11 |  b = Medium
 14  4 15 |  c = High

--------------
True Positive Rate:
0.6482758620689655
--------------
Precision:0.6752919247848254  Recall:0.6482758620689655