Summary:

Correctly Classified Instances          81               55.8621 %
Incorrectly Classified Instances        64               44.1379 %
Kappa statistic                          0.3737
Mean absolute error                      0.3062
Root mean squared error                  0.4963
Relative absolute error                 68.7647 %
Root relative squared error            105.0724 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 39  1  8 |  a = Low
  1 19  0 |  b = Medium
 15 39 23 |  c = High

--------------
True Positive Rate:
0.5586206896551724
--------------
Precision:0.6731453369691713  Recall:0.5586206896551724