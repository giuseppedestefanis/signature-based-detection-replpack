Summary:

Correctly Classified Instances          76               52.4138 %
Incorrectly Classified Instances        69               47.5862 %
Kappa statistic                          0.3207
Mean absolute error                      0.3287
Root mean squared error                  0.512 
Relative absolute error                 73.729  %
Root relative squared error            108.2733 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 43  0  5 |  a = Low
  2  0  2 |  b = Medium
  4 56 33 |  c = High

--------------
True Positive Rate:
0.5241379310344828
--------------
Precision:0.8196375791695988  Recall:0.5241379310344828