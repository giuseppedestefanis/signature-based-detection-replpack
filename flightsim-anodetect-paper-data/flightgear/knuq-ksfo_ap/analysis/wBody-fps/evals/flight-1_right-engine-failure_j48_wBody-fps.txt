Summary:

Correctly Classified Instances         102               70.3448 %
Incorrectly Classified Instances        43               29.6552 %
Kappa statistic                          0.5513
Mean absolute error                      0.2295
Root mean squared error                  0.4173
Relative absolute error                 51.6709 %
Root relative squared error             88.5776 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 23  5  0 |  a = Low
 21 28 10 |  b = Medium
  0  7 51 |  c = High

--------------
True Positive Rate:
0.7034482758620689
--------------
Precision:0.7201942545865667  Recall:0.7034482758620689