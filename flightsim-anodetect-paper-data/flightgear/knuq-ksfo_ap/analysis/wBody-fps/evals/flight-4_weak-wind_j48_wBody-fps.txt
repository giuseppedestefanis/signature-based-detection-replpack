Summary:

Correctly Classified Instances         113               77.931  %
Incorrectly Classified Instances        32               22.069  %
Kappa statistic                          0.6714
Mean absolute error                      0.1778
Root mean squared error                  0.3509
Relative absolute error                 40.0217 %
Root relative squared error             74.4423 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 43 11  0 |  a = Low
  2 31 18 |  b = Medium
  0  1 39 |  c = High

--------------
True Positive Rate:
0.7793103448275862
--------------
Precision:0.7981783649179082  Recall:0.7793103448275862