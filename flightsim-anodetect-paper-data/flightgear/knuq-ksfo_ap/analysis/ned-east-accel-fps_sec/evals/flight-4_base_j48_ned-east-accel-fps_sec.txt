Summary:

Correctly Classified Instances          94               64.8276 %
Incorrectly Classified Instances        51               35.1724 %
Kappa statistic                          0.468 
Mean absolute error                      0.2609
Root mean squared error                  0.4213
Relative absolute error                 58.7126 %
Root relative squared error             89.3782 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 25  6  9 |  a = Low
 15 26  9 |  b = Medium
  3  9 43 |  c = High

--------------
True Positive Rate:
0.6482758620689655
--------------
Precision:0.646438778137502  Recall:0.6482758620689655