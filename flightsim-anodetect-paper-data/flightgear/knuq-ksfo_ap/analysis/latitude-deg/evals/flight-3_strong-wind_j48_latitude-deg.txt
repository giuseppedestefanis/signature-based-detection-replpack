Summary:

Correctly Classified Instances         117               80.6897 %
Incorrectly Classified Instances        28               19.3103 %
Kappa statistic                          0.7057
Mean absolute error                      0.1287
Root mean squared error                  0.3588
Relative absolute error                 28.9729 %
Root relative squared error             76.1302 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 59  8  0 |  a = Low
  0 32 20 |  b = Medium
  0  0 26 |  c = High

--------------
True Positive Rate:
0.8068965517241379
--------------
Precision:0.8503148425787106  Recall:0.8068965517241379