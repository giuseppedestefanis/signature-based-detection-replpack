Summary:

Correctly Classified Instances         132               91.0345 %
Incorrectly Classified Instances        13                8.9655 %
Kappa statistic                          0.8652
Mean absolute error                      0.0598
Root mean squared error                  0.2445
Relative absolute error                 13.4498 %
Root relative squared error             51.8667 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 50  4  0 |  a = Low
  3 41  6 |  b = Medium
  0  0 41 |  c = High

--------------
True Positive Rate:
0.9103448275862069
--------------
Precision:0.912171787784684  Recall:0.9103448275862069