Summary:

Correctly Classified Instances         137               94.4828 %
Incorrectly Classified Instances         8                5.5172 %
Kappa statistic                          0.9173
Mean absolute error                      0.0368
Root mean squared error                  0.1918
Relative absolute error                  8.2768 %
Root relative squared error             40.6876 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 46  1  0 |  a = Low
  0 43  7 |  b = Medium
  0  0 48 |  c = High

--------------
True Positive Rate:
0.9448275862068966
--------------
Precision:0.9500313479623824  Recall:0.9448275862068966