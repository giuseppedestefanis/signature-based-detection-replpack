Summary:

Correctly Classified Instances          85               58.6207 %
Incorrectly Classified Instances        60               41.3793 %
Kappa statistic                          0.3651
Mean absolute error                      0.2818
Root mean squared error                  0.5077
Relative absolute error                 63.3646 %
Root relative squared error            107.613  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 36 15  0 |  a = Low
  7 13 17 |  b = Medium
 21  0 36 |  c = High

--------------
True Positive Rate:
0.5862068965517241
--------------
Precision:0.5833313969699787  Recall:0.5862068965517241