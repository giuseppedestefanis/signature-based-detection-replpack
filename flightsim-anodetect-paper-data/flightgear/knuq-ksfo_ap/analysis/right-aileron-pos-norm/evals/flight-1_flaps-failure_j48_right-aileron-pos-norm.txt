Summary:

Correctly Classified Instances          57               39.3103 %
Incorrectly Classified Instances        88               60.6897 %
Kappa statistic                          0.1465
Mean absolute error                      0.4027
Root mean squared error                  0.5961
Relative absolute error                 90.3434 %
Root relative squared error            126.0948 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 27 58 12 |  a = Low
  0  7  0 |  b = Medium
 16  2 23 |  c = High

--------------
True Positive Rate:
0.3931034482758621
--------------
Precision:0.6109046701422434  Recall:0.3931034482758621