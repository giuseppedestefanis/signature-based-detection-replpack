Summary:

Correctly Classified Instances          54               37.2414 %
Incorrectly Classified Instances        91               62.7586 %
Kappa statistic                         -0.0957
Mean absolute error                      0.3961
Root mean squared error                  0.53  
Relative absolute error                 88.8863 %
Root relative squared error            112.1291 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 20  2 24 |  a = Low
  1  0  8 |  b = Medium
 42 14 34 |  c = High

--------------
True Positive Rate:
0.3724137931034483
--------------
Precision:0.42046076528835147  Recall:0.3724137931034483