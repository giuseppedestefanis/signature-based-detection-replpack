Summary:

Correctly Classified Instances          89               61.3793 %
Incorrectly Classified Instances        56               38.6207 %
Kappa statistic                          0.4311
Mean absolute error                      0.3371
Root mean squared error                  0.4249
Relative absolute error                 75.7672 %
Root relative squared error             90.048  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 26 15 20 |  a = Low
  0 26  8 |  b = Medium
  3 10 37 |  c = High

--------------
True Positive Rate:
0.6137931034482759
--------------
Precision:0.6929967377054178  Recall:0.6137931034482759