Summary:

Correctly Classified Instances         133               91.7241 %
Incorrectly Classified Instances        12                8.2759 %
Kappa statistic                          0.8758
Mean absolute error                      0.0984
Root mean squared error                  0.2248
Relative absolute error                 22.1406 %
Root relative squared error             47.6934 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 43  4  0 |  a = Low
  5 42  1 |  b = Medium
  0  2 48 |  c = High

--------------
True Positive Rate:
0.9172413793103448
--------------
Precision:0.9178190241613886  Recall:0.9172413793103448