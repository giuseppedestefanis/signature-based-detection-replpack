Summary:

Correctly Classified Instances         122               84.1379 %
Incorrectly Classified Instances        23               15.8621 %
Kappa statistic                          0.7535
Mean absolute error                      0.1388
Root mean squared error                  0.315 
Relative absolute error                 31.2308 %
Root relative squared error             66.7951 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 60  6  2 |  a = Low
  2 35  8 |  b = Medium
  1  4 27 |  c = High

--------------
True Positive Rate:
0.8413793103448276
--------------
Precision:0.8490569387121111  Recall:0.8413793103448276