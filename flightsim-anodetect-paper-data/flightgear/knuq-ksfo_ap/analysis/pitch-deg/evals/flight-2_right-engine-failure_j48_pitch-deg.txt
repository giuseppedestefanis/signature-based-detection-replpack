Summary:

Correctly Classified Instances          94               64.8276 %
Incorrectly Classified Instances        51               35.1724 %
Kappa statistic                          0.4858
Mean absolute error                      0.2557
Root mean squared error                  0.4651
Relative absolute error                 57.6128 %
Root relative squared error             98.7813 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 30  4  5 |  a = Low
 11 27 28 |  b = Medium
  0  3 37 |  c = High

--------------
True Positive Rate:
0.6482758620689655
--------------
Precision:0.7040772911351251  Recall:0.6482758620689655