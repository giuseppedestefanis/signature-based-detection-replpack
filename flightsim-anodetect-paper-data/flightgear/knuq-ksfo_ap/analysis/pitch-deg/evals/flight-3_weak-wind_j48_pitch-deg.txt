Summary:

Correctly Classified Instances         123               84.8276 %
Incorrectly Classified Instances        22               15.1724 %
Kappa statistic                          0.7726
Mean absolute error                      0.133 
Root mean squared error                  0.2994
Relative absolute error                 29.926  %
Root relative squared error             63.513  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 45  7  4 |  a = Low
  0 42  7 |  b = Medium
  0  4 36 |  c = High

--------------
True Positive Rate:
0.8482758620689655
--------------
Precision:0.8652999072523152  Recall:0.8482758620689655