Summary:

Correctly Classified Instances         100               68.9655 %
Incorrectly Classified Instances        45               31.0345 %
Kappa statistic                          0.5334
Mean absolute error                      0.2694
Root mean squared error                  0.3975
Relative absolute error                 60.5936 %
Root relative squared error             84.2789 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 35 13  0 |  a = Low
 19 20  2 |  b = Medium
  2  9 45 |  c = High

--------------
True Positive Rate:
0.6896551724137931
--------------
Precision:0.7113160744855536  Recall:0.6896551724137931