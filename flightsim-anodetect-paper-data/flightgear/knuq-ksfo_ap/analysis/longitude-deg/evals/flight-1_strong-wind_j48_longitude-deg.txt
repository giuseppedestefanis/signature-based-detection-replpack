Summary:

Correctly Classified Instances         122               84.1379 %
Incorrectly Classified Instances        23               15.8621 %
Kappa statistic                          0.7566
Mean absolute error                      0.1057
Root mean squared error                  0.3252
Relative absolute error                 23.7975 %
Root relative squared error             68.994  %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 28  0  0 |  a = Low
 20 28  3 |  b = Medium
  0  0 66 |  c = High

--------------
True Positive Rate:
0.8413793103448276
--------------
Precision:0.8997501249375313  Recall:0.8413793103448276