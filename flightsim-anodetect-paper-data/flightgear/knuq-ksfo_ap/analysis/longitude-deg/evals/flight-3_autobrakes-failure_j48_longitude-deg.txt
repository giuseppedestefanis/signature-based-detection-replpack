Summary:

Correctly Classified Instances         141               97.2414 %
Incorrectly Classified Instances         4                2.7586 %
Kappa statistic                          0.9586
Mean absolute error                      0.0184
Root mean squared error                  0.1356
Relative absolute error                  4.1381 %
Root relative squared error             28.7685 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 46  2  0 |  a = Low
  0 48  1 |  b = Medium
  0  1 47 |  c = High

--------------
True Positive Rate:
0.9724137931034482
--------------
Precision:0.9732251521298175  Recall:0.9724137931034482