Summary:

Correctly Classified Instances         133               91.7241 %
Incorrectly Classified Instances        12                8.2759 %
Kappa statistic                          0.8756
Mean absolute error                      0.0552
Root mean squared error                  0.2349
Relative absolute error                 12.4161 %
Root relative squared error             49.8354 %
Total Number of Instances              145     

--------------
Confusion matrix:
=== Confusion Matrix ===

  a  b  c   <-- classified as
 39  0  0 |  a = Low
  8 43  0 |  b = Medium
  0  4 51 |  c = High

--------------
True Positive Rate:
0.9172413793103448
--------------
Precision:0.9242846661775495  Recall:0.9172413793103448